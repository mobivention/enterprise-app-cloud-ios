Pod::Spec.new do |s|
  s.name = "EnterpriseAppCloudAPI"
  s.version = "1.0.1"
  s.summary = "Check for Updates on Enteprise App Cloud"

  # This description is used to generate tags and improve search results.
  #   * Think: What does it do? Why did you write it? What is the focus?
  #   * Try to keep it short, snappy and to the point.
  #   * Write the description between the DESC delimiters below.
  #   * Finally, don't worry about the indent, CocoaPods strips it!
  s.description = <<-DESC

    This library can be integrated into apps, that are distributed via Enterprise App Cloud
    (https://enterprise-app-cloud.com), to check for updates and block any UI in case of
      mandatory updates
                   DESC

  s.homepage = "https://enterprise-app-cloud.com"
  s.license = { :type => "MIT", :file => "LICENSE" }
  s.author = { "Sören Busch" => "sbusch@mobivention.com" }

  s.platform = :ios, "9.0"
  s.swift_version = "5.0"
  s.source = { :git => "https://sbusch@bitbucket.org/mobivention/enterprise-app-cloud-ios.git", :tag => "#{s.version}" }

  s.source_files = "EACApi/EACApi.swift"
end
