//
//  EACApi.swift
//  EnterpriseAppCloudAPI
//
//  Created by Sören Busch on 12/12/16.
//  Copyright © 2016 mobivention. All rights reserved.
//

import Foundation

/// A Callback to use for handling available updates
public typealias UpdateHandler = (_ version: String, _ build: String, _ downloadURL: URL, _ iconURL: URL?) -> Void

private class UpdateView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.white
        setup()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        backgroundColor = UIColor.white
        setup()
    }
    
    var titleLabel: UILabel?
    var title = "" {
        didSet {
            titleLabel?.text = title
        }
    }
    
    var textLabel: UILabel?
    var text = "" {
        didSet {
            textLabel?.text = text
        }
    }
    
    var installButton: UIButton?
    var installURL: URL?
    
    func setup() {
        titleLabel = UILabel()
        titleLabel?.textAlignment = .center
        titleLabel?.font = UIFont.systemFont(ofSize: 24)
        addSubview(titleLabel!)
        
        textLabel = UILabel()
        textLabel?.textAlignment = .center
        textLabel?.font = UIFont.systemFont(ofSize: 16)
        textLabel?.numberOfLines = 0
        addSubview(textLabel!)
        
        installButton = UIButton()
        installButton?.setTitle(NSLocalizedString("Install", comment: ""), for: .normal)
        installButton?.addTarget(self, action: #selector(install), for: .touchUpInside)
        installButton?.setTitleColor(UIColor.blue, for: .normal)
        installButton?.contentHorizontalAlignment = .center
        addSubview(installButton!)
        
    }
    
    @objc func install() {
        if let url = installURL {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    override func didMoveToWindow() {
        super.didMoveToWindow()
        self.frame = window?.bounds ?? CGRect.zero
        
        // We should not need to use SnapKit here... but apparently i am too dumb to use native NSLayoutConstraints (also they are annoying)
        if let label = titleLabel {
            label.translatesAutoresizingMaskIntoConstraints = false
            var left = NSLayoutConstraint(item: label, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1.0, constant: 16)
            var right = NSLayoutConstraint(item: label, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1.0, constant: -16)
            var top = NSLayoutConstraint(item: label, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 32.0)
            self.addConstraints([left, right, top])
            
            textLabel?.translatesAutoresizingMaskIntoConstraints = false
            left = NSLayoutConstraint(item: label, attribute: .left, relatedBy: .equal, toItem: textLabel, attribute: .left, multiplier: 1.0, constant: 0)
            right = NSLayoutConstraint(item: label, attribute: .right, relatedBy: .equal, toItem: textLabel, attribute: .right, multiplier: 1.0, constant: 0)
            top = NSLayoutConstraint(item: label, attribute: .bottom, relatedBy: .equal, toItem: textLabel, attribute: .top, multiplier: 1.0, constant: -16.0)
            
            self.addConstraints([left, right, top])
            
            if let textLabel = textLabel {
                installButton?.translatesAutoresizingMaskIntoConstraints = false
                let width = NSLayoutConstraint(item: textLabel, attribute: .width, relatedBy: .equal, toItem: installButton, attribute: .width, multiplier: 2.0, constant: 0)
                let centerX = NSLayoutConstraint(item: textLabel, attribute: .centerX, relatedBy: .equal, toItem: installButton, attribute: .centerX, multiplier: 1.0, constant: 0)
                top = NSLayoutConstraint(item: textLabel, attribute: .bottom, relatedBy: .equal, toItem: installButton, attribute: .top, multiplier: 1.0, constant: -16.0)
                
                self.addConstraints([width, centerX, top])
            }
            
        }
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.frame = window?.bounds ?? CGRect.zero
    }
}

/** Use this class to check for optional or mandatory updates on the cloud
 
 Simple Use Case:
 
 func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
 EACApi.checkForUpdates()
 return true
 }
 
 More Customized Use Case:
 
 EACApi.checkForUpdates(baseURL: URL(string: "https://enterprise-app-cloud.com")!,
 enable: true,
 optionalUpdateHandler: {
 version, build, downloadURL, iconURL in
 
 // Display custom Notification for optional Update
 },
 mandatoryUpdateHandler: {
 version, build, downloadURL, iconURL in
 
 // Display custom Notification for mandatory Update
 })
 
 If you want to keep using the default behaviour but just customize your text, you can also add the following to your Localizable.strings:
 
 "An update to version %@ (%@) is available. Install now?" = "An update to version %1$@ (%2$@) is available. Install now?";
 "An update to version %@ (%@) is available. This update has been marked as mandatory. Install now?" = "An update to version %1$@ (%2$@) is available. This update has been marked as mandatory. Install now?";
 "Install" = "Install";
 "No" = "No";
 "Update available" = "Update available";
 "Yes" = "Yes";
 
 */


public class EACApi {
    
    static let titleString = NSLocalizedString("Update available", comment: "")
    static let optionalMessageString = NSLocalizedString("An update to version %@ (%@) is available. Install now?", comment: "")
    static let mandatoryMessageString = NSLocalizedString("An update to version %@ (%@) is available. This update has been marked as mandatory. Install now?", comment:"")
    static let yesString = NSLocalizedString("Yes", comment: "")
    static let noString = NSLocalizedString("No", comment: "")
    
    private static var optionalUpdateHandler: UpdateHandler = {
        (version, build, downloadURL, iconURL) -> Void in
        let ac = UIAlertController(title: titleString, message: String(format: optionalMessageString, version, build), preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: noString, style: .cancel, handler: { (_) in })
        let yesAction = UIAlertAction(title: yesString, style: .default, handler: { (_) in
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(downloadURL, options: [:], completionHandler: { _ in })
            } else {
                UIApplication.shared.openURL(downloadURL)
            }
        })
        
        ac.addAction(cancelAction)
        ac.addAction(yesAction)
        
        UIApplication.shared.keyWindow?.rootViewController?.present(ac, animated: true, completion: {})
    }
    
    private static var window: UIWindow?
    
    private static var mandatoryUpdateHandler: UpdateHandler = {
        (version, build, downloadURL, iconURL) -> Void in
        
        let view = UpdateView()
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.addSubview(view)
        window?.makeKeyAndVisible()
        view.title = titleString
        view.text = String(format: mandatoryMessageString, version, build)
        view.installURL = downloadURL
    }
    
    /**
     Check the enterprise app cloud for any available updates to your app
     
     - parameter baseURL:                Base URL of Enterprise App Cloud (default: "https://enterprise-app-cloud.com")
     - parameter optionalUpdateHandler:  A Callback to use for displaying an optional update. If nil, it will default to displaying an alert view
     - parameter mandatoryUpdateHandler: A Callback to use for displaying a mandatory update. If nil, it will default to displaying a full screen view that links to the update
     - parameter enable:            Actually enable the check (should usually be disabled when debugging)
     */
    public static func checkForUpdates(baseURL: URL = URL(string: "https://enterprise-app-cloud.com")!,
                                       enable: Bool = true,
                                       optionalUpdateHandler: UpdateHandler? = nil,
                                       mandatoryUpdateHandler: UpdateHandler? = nil
        ) {
        let o = optionalUpdateHandler ?? self.optionalUpdateHandler
        let m = mandatoryUpdateHandler ?? self.mandatoryUpdateHandler
        let bundleId = Bundle.main.bundleIdentifier ?? ""
        let url = baseURL.appendingPathComponent("/api/ios/\(bundleId)/current/")
        
        let session = URLSession(configuration: URLSessionConfiguration.default, delegate: nil, delegateQueue: OperationQueue.main)
        
        let task = session.dataTask(with: url, completionHandler: {
            data, response, error in
            do {
                if let data = data,
                    let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any],
                    let app = json["app"] as? [String:Any] {
                    let downloadURI = json["download_url"] as? String
                    let iconURI = app["icon_url"] as? String
                    let mandatory = json["mandatory"] as? Bool ?? false
                    let build = json["build"] as? String
                    let version = json["version"] as? String
                    
                    if build != Bundle.main.infoDictionary?[kCFBundleVersionKey as String] as? String && enable{
                        
                        if let downloadURL = URL(string: downloadURI ?? ""),
                            let build = build,
                            let version = version {
                            
                            if mandatory {
                                m(version, build, downloadURL, URL(string: iconURI ?? ""))
                            }
                            else {
                                o(version, build, downloadURL, URL(string: iconURI ?? ""))
                            }
                            
                        }
                    }
                    
                }
            }
            catch _ {
                
            }
        }
            
        )
        
        task.resume()
        
        
    }
}
